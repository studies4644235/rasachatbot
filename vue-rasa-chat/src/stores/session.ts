import { defineStore } from 'pinia'
import { Message, MessageType, TextBlock } from '@/message'
import { ref } from 'vue'
import { nanoid } from 'nanoid'
import type { RasaQueryResult, RasaSearchHistory, RasaSlots, UnknownRasaPayload } from '@/rasa'

export const useSessionStore = defineStore(
  'session',
  () => {
    const messages = ref([] as Message[])
    const sender = ref('')

    function hasSession(): boolean {
      return sender.value ? true : false
    }

    function newSession() {
      messages.value = [] as Message[]
      sender.value = nanoid()
    }

    function addMessage(message: Message) {
      messages.value.push(message)
    }

    return { hasSession, newSession, addMessage, messages, sender }
  },
  {
    persist: {
      debug: true,
      afterRestore: (ctx) => {
        const messages = ctx.store.messages
        const restoredMessages: Message[] = []

        messages.forEach(
          (message: {
            body: TextBlock[]
            type: MessageType
            time: string
            payload?: { type: string; data: Object }
          }) => {
            if (message.type === MessageType.Bot && message.payload !== undefined) {
              restoredMessages.push(
                new Message(
                  message.body,
                  message.type,
                  message.payload as
                    | RasaQueryResult
                    | RasaSearchHistory
                    | RasaSlots
                    | UnknownRasaPayload,
                  new Date(message.time)
                )
              )
            } else {
              restoredMessages.push(
                new Message(message.body, message.type, undefined, new Date(message.time))
              )
            }
          }
        )

        ctx.store.messages = restoredMessages
      }
    }
  }
)
