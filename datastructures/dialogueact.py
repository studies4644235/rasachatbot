from collections import defaultdict


class DialogueAct(object):
    intent = ""
    slots = defaultdict(str)

    def __init__(self, intent: str = None, slots: dict = None):
        if intent is not None:
            self.intent = intent
        if slots is not None:
            self.slots.update(slots)

    def create(self, intent: str, slots: dict):
        pass
