from datastructures.dialoguestate import DialogueState
from datastructures.dialogueact import DialogueAct


class DialogueDecider(object):
    def __init__(self):
        pass

    def actOn(self, state: DialogueState) -> DialogueAct:
        return DialogueAct("ask", {"foodtype": None})
