from datastructures.dialoguestate import DialogueState
from languageunderstanding.understandingunits import SimpleNLU
from dialoguemanagement.stateupdate import SimpleStateUpdater
from dialoguemanagement.dialoguedecision import DialogueDecider
from languagegeneration.generators import SimpleGenerator

import argparse


class DialogueAgent:
    asr = None
    nlu = None
    stateUpdate = None
    dialogDecision = None
    nlg = None
    tts = None

    def __init__(self):
        self.nlu = SimpleNLU()
        self.stateUpdate = SimpleStateUpdater()
        self.dialogDecision = DialogueDecider()
        self.nlg = SimpleGenerator()

    def _dialoguePipeline(self, inputText: str, state: DialogueState) -> str:
        inputAction = self.nlu.parse(inputText)
        state = self.stateUpdate.update(state, inputAction)
        systemAction = self.dialogDecision.actOn(state)
        output = self.nlg.generate(systemAction)
        return output

    def runText(self):
        state = None
        inputText = input("User > ")
        while not self._isBye(inputText):
            output = self._dialoguePipeline(inputText, state)

            print("Sys  >", output)
            inputText = input("User > ")

    def runAudio(self):
        from speechsynthesis.synthesisers import MicrosoftTTSConnector
        from speechrecognition.recognizers import MicrosoftASRConnector

        if self.asr == None:
            self.asr = MicrosoftASRConnector()
        if self.tts == None:
            self.tts = MicrosoftTTSConnector()

        state = None
        print("Start speaking...")
        inputText = self.asr.recognize()
        print("\tUser > {}".format(inputText))
        while not self._isBye(inputText):
            output = self._dialoguePipeline(inputText, state)

            print("\tSys  >", output)
            self.tts.synthesise(output)
            print()
            print("Start speaking...")
            inputText = self.asr.recognize()
            print("\tUser > {}".format(inputText))

    def _isBye(self, text: str) -> bool:
        if text is None:
            return True
        if text.lower().strip() in ["bye", "exit", "abort", "cancel"]:
            return True
        text = text.lower().strip()
        return "bye" in text or "exit" in text or "abort" in text or "cancel" in text


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--type",
        type=str,
        help="type of interaction, one out of 'voice' or 'text'.",
        default="text",
    )

    args = parser.parse_args()

    agent = DialogueAgent()
    if args.type == "voice":
        agent.runAudio()
    elif args.type == "text":
        agent.runText()
    else:
        parser.print_help()
