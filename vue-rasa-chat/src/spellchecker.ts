import axios from 'axios'

export type TokenSuggestion = {
  score: number
  suggestion: string
}

export type FlaggedToken = {
  offset: number
  suggestions: TokenSuggestion[]
  token: string
  type: string
}

export type SpellCheck = {
  _type: 'SpellCheck'
  flaggedTokens: FlaggedToken[]
}

export type ErrorResponse = {
  _type: 'ErrorResponse'
  errors: BError[]
}

export type BError = {
  code: string
  message: string
  moreDetails: string
  parameter: string
  subCode: string
  value: string
}

export async function spellCheck(text: string): Promise<SpellCheck | undefined> {
  try {
    const response = await axios({
      method: 'post',
      url: 'https://api.bing.microsoft.com/v7.0/SpellCheck',
      data: {
        text: text,
        sender: 'User'
      },
      params: {
        mkt: 'en-us',
        mode: 'proof'
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Ocp-Apim-Subscription-Key': import.meta.env.VITE_BING_SPELLCHECKING_API_KEY
      }
    })

    if (response.data._type === 'SpellCheck') {
      return response.data as SpellCheck
    } else if (response.data._type === 'ErrorResponse') {
      return undefined
    }
  } catch (err) {
    return undefined
  }
}

// export function applySpellCheck(text: string, spellCheck: SpellCheck): TextBlock[] {
//   const textSplit = text.split(/\S+/)
//   const curFlaggedToken = 0
// }
