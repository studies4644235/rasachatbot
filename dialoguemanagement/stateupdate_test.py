import unittest
from datastructures.dialogueact import DialogueAct
from datastructures.dialoguestate import DialogueState

from dialoguemanagement.stateupdate import SimpleStateUpdater


class TestStateUpdate(unittest.TestCase):
    def setUp(self):
        self.stateUpdate = SimpleStateUpdater()

    def test_update(self):
        # DialogueState({"intent":"find.restaurant","area":"centre", "pricerange":"cheap"})
        state = DialogueState()
        state = self.stateUpdate.update(
            act=DialogueAct(
                intent="find.restaurant",
                slots={"area": "centre", "pricerange": "cheap"},
            ),
            state=state,
        )

        self.assertIsInstance(state, DialogueState)
        self.assertIn("intent", state)
        self.assertIn("area", state)
        self.assertIn("pricerange", state)
        self.assertEqual("find.restaurant", state["intent"])
        self.assertEqual("centre", state["area"])
        self.assertEqual("cheap", state["pricerange"])

        self.assertIsInstance(state.resultList, list)
        self.assertEqual(len(state.resultList), 15)
