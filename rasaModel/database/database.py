import sqlite3, json


class Database(object):
    dbfile = "resources/CamRestaurants-dbase.db"
    ontologyFile = "resources/CamRestaurants-rules.json"
    ontology = None
    db_connection = None
    cursor = None

    def __init__(self):
        try:
            self.db_connection = sqlite3.connect(self.dbfile)
            self.db_connection.row_factory = (
                self._dict_factory
            )  # for getting entities back as python dict's
            self.cursor = self.db_connection.cursor()
        except Exception as e:
            print(e)
            print("Could not load database file: {}}".format(self.dbfile))
            exit(1)

        try:
            with open(self.ontologyFile) as ontofile:
                self.ontology = json.load(ontofile)
        except IOError:
            print("No such file or directory: " + self.ontologyFile)
            exit()
        return

    def findRestaurants(self, constraints: dict) -> dict:
        query = "select * from CamRestaurants where "

        consts = []

        for slot in self.ontology["slots"]:
            if slot in constraints and constraints[slot] is not None:
                slot_consts = []
                for constrain in constraints[slot]:
                    slot_consts.append(
                        '{} = "{}" COLLATE NOCASE'.format(slot, constrain)
                    )

                query_part = ""
                if len(slot_consts):
                    query_part += " or ".join(slot_consts)
                    consts.append("({})".format(query_part))

        if len(consts):
            query += " and ".join(consts)
        else:
            query += "1"

        print(query)
        self.cursor.execute(query)
        results = self.cursor.fetchall()

        return results

    def findRandomRestaurant(self) -> dict:
        self.cursor.execute("SELECT * FROM CamRestaurants ORDER BY RANDOM() LIMIT 1;")
        return self.cursor.fetchall()

    def _dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
