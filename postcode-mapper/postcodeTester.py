import pandas as pd

distances = pd.read_csv("distances.csv")


def get_postcodes_in_radius(postcode, radius):
    dists = distances[["postcode", postcode]].sort_values(by=postcode)
    dists = dists[dists[postcode].between(0, radius)]

    print(
        "The closest postcodes in a radius of {} m for {} are:\n {}".format(
            radius, postcode, dists
        )
    )

    return dists


def get_n_closest_postcodes(postcode, n):
    dists = distances[["postcode", postcode]].sort_values(by=postcode)
    dists = dists.head(n)

    print("The {} closest postcodes for {} are:\n {}".format(n, postcode, dists))

    return dists


if __name__ == "__main__":
    get_postcodes_in_radius("CB2 1UW", 1000)
    get_n_closest_postcodes("CB1 9HX", 3)
