import type { RasaQueryResult, RasaSearchHistory, RasaSlots, UnknownRasaPayload } from './rasa'

export class Message {
  readonly body: TextBlock[]
  readonly type: MessageType
  readonly time: Date
  readonly payload: RasaQueryResult | RasaSearchHistory | RasaSlots | UnknownRasaPayload | undefined

  constructor(
    body: TextBlock[],
    type: MessageType,
    payload:
      | RasaQueryResult
      | RasaSearchHistory
      | RasaSlots
      | UnknownRasaPayload
      | undefined = undefined,
    time: Date = new Date()
  ) {
    this.body = body
    this.type = type
    this.time = time
    this.payload = payload
  }

  getCorrected(): string {
    return this.body
      .map((textBlock) => {
        if (textBlock.hasError) {
          return textBlock.correction
        }
        return textBlock.original
      })
      .join('')
  }

  getOriginal(): string {
    return this.body
      .map((textBlock) => {
        return textBlock.original
      })
      .join('')
  }

  getHTML(): string {
    return this.body
      .map((textBlock) => {
        if (textBlock.hasError) {
          return '<s>' + textBlock.original + '</s>' + ' ' + '<i>' + textBlock.correction + '</i>'
        } else {
          return textBlock.original
        }
      })
      .join('')
  }
}

export enum MessageType {
  Bot = 1,
  User,
  System
}

export class TextBlock {
  original: string
  correction: string | null
  hasError: boolean

  constructor(original: string, correction: string | null) {
    this.original = original
    this.correction = correction

    correction === null ? (this.hasError = false) : (this.hasError = true)
  }
}
