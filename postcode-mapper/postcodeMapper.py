import sqlite3
import pandas as pd
import geopy.distance

raw = pd.read_csv("ukpostcodes.csv")

con = sqlite3.connect("CamRestaurants-dbase.db")

cur = con.cursor()

res = cur.execute("SELECT DISTINCT postcode FROM CamRestaurants")
postcodes = res.fetchall()

postcodes_coords = pd.DataFrame(columns=["postcode", "latitude", "longitude"])

for postcode in postcodes:
    postcode = postcode[0]
    postcode = postcode.replace(" ", "")
    postcode = postcode.replace(".", "")
    postcode = postcode.replace(",", " ")
    postcode = postcode.upper()
    coord = raw.loc[raw["postcode"] == postcode]

    new_coord = pd.DataFrame(
        [[postcode, coord.latitude, coord.longitude]],
        columns=["postcode", "latitude", "longitude"],
    )

    postcodes_coords = pd.concat([postcodes_coords, new_coord], ignore_index=True)

postcols = list(postcodes_coords["postcode"])
postcols.insert(0, "postcode")

postcodes_dist = pd.DataFrame(columns=postcols)

for index1, row1 in postcodes_coords.iterrows():
    postcode1 = row1["postcode"]
    latitude1 = row1["latitude"].values[0]
    longitude1 = row1["longitude"].values[0]

    new_dists = [postcode1]

    for index2, row2 in postcodes_coords.iterrows():
        postcode2 = row2["postcode"]
        latitude2 = row2["latitude"].values[0]
        longitude2 = row2["longitude"].values[0]

        dist = geopy.distance.geodesic(
            (latitude1, longitude1), (latitude2, longitude2)
        ).m
        # print(latitude1, longitude1, latitude2, longitude2)
        new_dists.append(dist)

    new_dists = pd.DataFrame([new_dists], columns=postcols)

    postcodes_dist = pd.concat([postcodes_dist, new_dists], ignore_index=True)

print(postcodes_dist)

postcodes_dist.to_csv("distances.csv")
