import re


class DataChecker:
    _status = 0
    invalid = []
    filepath = ""
    data = []
    _pattern = r"\[[\s*a-zA-Z0-9._-]+\]\s*[\{\"[a-zA-Z0-9._-]+\":\"[a-zA-Z0-9._-]+\"(,\"[a-zA-Z0-9._-]+\":\"[a-zA-Z0-9._-]+\")*}|\[[\s*a-zA-Z0-9._-]+\]\s*[\{\"[a-zA-Z0-9._-]+\":\"[a-zA-Z0-9._-]+\"}|\[[\s*a-zA-Z0-9._-]+\]\s*\([\s*a-zA-Z0-9._-\_*\w*]+\)"
    _outfile = "invalidLines.txt"
    _line = 0

    def __init__(self):
        pass

    def set_outfile(self, outfile):
        self._outfile = outfile

    def file_reader(self):
        try:
            with open(self.filepath) as file:
                self.data = file.read().splitlines()
        except FileNotFoundError:
            raise FileNotFoundError

    def _set_filepath(self):
        self.filepath = input("Enter the filepath to the nlu training file.")

    def text_validity_checker(self):
        for sentence in self.data:
            self._line = self._line + 1
            if "[" in sentence or "]" in sentence or "{" in sentence or "}" in sentence:
                pass
            else:
                continue
            matches = re.findall(self._pattern, sentence)
            if matches or sentence == "":
                continue
            else:
                self.invalid.append(sentence + f"[{self._line}]")
        if len(self.invalid) != 0:
            self._status = -1
        self._line = 0

    def write_to_file(self):
        with open(self._outfile, "w") as file:
            for sentence in self.invalid:
                sentence = sentence.lstrip()
                self.invalid = file.write(sentence + "\n")

    @staticmethod
    def check_attribute():
        instance = DataChecker()
        if instance._status == -1:
            exit(-1)
