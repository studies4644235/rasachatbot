FROM python:3.10.9

WORKDIR /app

COPY . /app

ADD endpoints-docker.yml endpoints.yml

RUN pip install -r requirements-docker.txt

EXPOSE 5005

ENTRYPOINT rasa run -m models --enable-api --cors "*" --debug