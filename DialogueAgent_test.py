import unittest

from DialogueAgent import DialogueAgent
from datastructures.dialoguestate import DialogueState


class TestStateUpdate(unittest.TestCase):
    def setUp(self):
        self.agent = DialogueAgent()

    def test_runPipeline(self):
        input = "I am looking for a cheap restaurant in the city centre."
        state = DialogueState()
        output = self.agent._dialoguePipeline(input, state)

        self.assertIsInstance(output, str)
        self.assertEqual(output, "What foodtype are you looking for?")
