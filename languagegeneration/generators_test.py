import unittest
from datastructures.dialogueact import DialogueAct
from languagegeneration.generators import SimpleGenerator


class TestNLG(unittest.TestCase):
    def setUp(self):
        self.nlg = SimpleGenerator()

    def test_parse(self):
        systemaction = DialogueAct("ask", {"foodtype": None})
        output = self.nlg.generate(systemaction)

        self.assertIsInstance(output, str)
        self.assertEqual("What foodtype are you looking for?", output)
