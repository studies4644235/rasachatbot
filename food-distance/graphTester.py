import igraph as ig
import pandas as pd

g = ig.Graph.Read_GraphMLz("graph.graphmlz")


def get_closest(source_name):
    source = g.vs.select(name=source_name)
    targets = g.vs.select(db=True)

    source = list(map(lambda v: v.index, source))
    targets = list(map(lambda v: v.index, targets))

    dists = g.distances(source=source, target=targets, weights="distance", mode="all")

    df = pd.DataFrame(columns=["name", "distance"])

    for index, dist in enumerate(dists[0]):
        target = targets[index]
        name = g.vs[target]["name"]

        df = pd.concat(
            [pd.DataFrame([[name, dist]], columns=df.columns), df], ignore_index=True
        )

    df = df.drop(df[df["name"] == source_name].index)

    df = df.sort_values(["distance"])

    return df


print(get_closest("german"))
