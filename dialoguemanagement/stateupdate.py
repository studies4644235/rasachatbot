from datastructures.dialoguestate import DialogueState
from datastructures.dialogueact import DialogueAct
from database.database import Database


class SimpleStateUpdater(object):
    database = None

    def __init__(self):
        self.database = Database()

    def update(self, state: DialogueState, act: DialogueAct) -> DialogueState:
        newState = DialogueState(
            {"intent": "find.restaurant", "area": "centre", "pricerange": "cheap"}
        )

        if "intent" in newState and "restaurant" in newState["intent"]:
            newState.resultList = self.database.findRestaurants(newState)

        return newState
