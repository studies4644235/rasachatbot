import os
import unittest

import utility.nlu_data_linting.DataChecker as dc


class test_class(unittest.TestCase):
    _incorrectText = """- my name is [Bob]{user_name)[270]
        - I am [Peter]{user_name)[271]
        - my friends call me [juicer]{user_name)[272]
        - Show me the last [20](count) search results[332]
        - wrong. Show me the last [20](count) search results[346]
        - wrong. Which restaurants are located in a [range of 10 km](location", "role":"distance) from here?[373]
        - stop. Which restaurants are located in a [range of 10 km](location", "role":"distance) from here?[395]
        - I say no. Which restaurants are located in a [range of 10 km](location", "role":"distance) from here?[417]
        - Which restaurants are located in a [range of 10 km](location", "role":"distance) from here?. Give me the [contact information]({"entity":"contact","role":"adress"}", "role":"multi_{"entity":"contact","role":"adress"}) to this restaurant.[434]
        - Can you suggest a restaurant that's [close to me](location", "role":"relative_position)?. Give me the [contact information]({"entity":"contact","role":"adress"}", "role":"multi_{"entity":"contact","role":"adress"}) to this restaurant.[472]
        - this is what I wanted. Print all the [information]({"entity":"contact","role":"adress"}", "role":"multi_{"entity":"contact","role":"adress"}) about this restaurant.[499]
        - absolutely. Give me the [contact information]({"entity":"contact","role":"adress"}", "role":"multi_{"entity":"contact","role":"adress"}) to this restaurant.[506]"""
    _correctText = """- my friends call me [juicer](user_name)
        - No. Where should I go for a [great](description) meal?
        - wrong. I'm looking for a restaurant recommendation.
        - how could you. Any hidden gems you can recommend?
        - What's the balance on my [credit card account]{"entity":"account","value":"credit"}
        """
    filepath = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        "rasaModel/data/nlu.yml",
    )

    def test_DataCheckerIncorrectData(self):
        testChecker = dc.DataChecker()
        testChecker.data = test_class._incorrectText.splitlines()
        testChecker.text_validity_checker()
        self.assertEqual(testChecker._status, -1)

    def test_DataCheckerCorrectData(self):
        testCorrect = dc.DataChecker()
        testCorrect.data = test_class._correctText.splitlines()
        testCorrect.text_validity_checker()
        self.assertEqual(testCorrect._status, 0)

    def test_NLUData(self):
        nluDataChecker = dc.DataChecker()
        nluDataChecker.filepath = test_class.filepath
        nluDataChecker.file_reader()
        nluDataChecker.text_validity_checker()
        if nluDataChecker._status != 0:
            print(nluDataChecker.invalid)
        self.assertEqual(nluDataChecker._status, 0)
