# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List, Tuple

import igraph as ig
import pandas as pd
import rasa_sdk.events
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, FollowupAction
from rasa_sdk.executor import CollectingDispatcher

from database.database import Database

rest_database = Database()
distances = pd.read_csv("resources/post-code-distances.csv")
food_graph = ig.Graph.Read_GraphMLz("resources/food-graph.graphmlz")


class ActionQueryRandom(Action):
    def name(self) -> Text:
        return "action_query_random"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        print("Old slots:")
        print(tracker.slots)
        result = rest_database.findRandomRestaurant()
        past_results = tracker.get_slot("past_results")
        try:
            if tracker.get_slot("past_results") is not None:
                past_results = past_results + [result]
            else:
                past_results = result
        except TypeError:
            dispatcher.utter_message(
                "I encountered an internal error and could update the past history correctly."
            )
            pass
        return [SlotSet("query_result", result), SlotSet("past_results", past_results)]


class ActionQueryDb(Action):
    def name(self) -> Text:
        return "action_query_db"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        # dispatcher.utter_message(
        #     "Current slots are: " + str(tracker.latest_message["entities"])
        # )
        print(tracker.slots)
        query_filters = tracker.slots.copy()

        if query_filters["postcode"]:
            normalized_postcodes = []
            for pcode in query_filters["postcode"]:
                normalized_postcodes.append(pcode.replace(" ", ""))
            query_filters["postcode"] = normalized_postcodes

        available_restaurants = rest_database.findRestaurants(query_filters)

        # directly call followup action find alternative food if nothing has been found, without waiting for user to affirm
        if available_restaurants is None:
            dispatcher.utter_message(
                "I couldn't find a fitting restaurant. Looking for alternatives..."
            )
            return [FollowupAction("action_suggest_filter_based_on_slots")]
        past_results = tracker.get_slot("past_results")
        try:
            if tracker.get_slot("past_results") is not None:
                past_results = tracker.get_slot("past_results") + [
                    available_restaurants
                ]
            else:
                past_results = available_restaurants
        except TypeError:
            dispatcher.utter_message(
                "I encountered an internal error and could update the past history correctly."
            )
            pass
        return [
            SlotSet("query_result", available_restaurants),
            SlotSet("past_results", past_results),
        ]


class ActionGetNearestPostcode(Action):
    def name(self) -> Text:
        return "action_get_nearest_postcode"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(str(get_postcodes_in_radius("CB2 1UW", 1000)))
        return []


class ActionSuggestAlternativeFood(Action):
    def name(self) -> Text:
        return "action_suggest_alternative_food"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        valid_alternative = False
        alternative_foods = get_closest_food(tracker.slots["food"][0])
        cur_idx = 0
        while not valid_alternative and cur_idx < len(alternative_foods):
            current_slots = tracker.slots.copy()
            current_slots["food"][0] = str(alternative_foods.iloc[cur_idx]["name"])
            available_restaurants = rest_database.findRestaurants(current_slots)
            if len(available_restaurants) == 0 and len(current_slots["food"]) > 0:
                cur_idx += 1
            else:
                valid_alternative = True

        if cur_idx >= len(alternative_foods):
            dispatcher.utter_message("Sorry i didn't find a good alternative.")
        else:
            dispatcher.utter_message(
                "I found a similar type of food. Is "
                + str(alternative_foods.iloc[cur_idx]["name"])
                + " also ok for you?"
            )
            # TODO: Only question here, need to look into how to handle response
        return []


class ActionSuggestAlternativePricerange(Action):
    def name(self) -> Text:
        return "action_suggest_alternative_pricerange"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        valid_alternative = False
        priceranges = {"cheap": 1, "moderate": 2, "expensive": 3}
        current_pricerange = priceranges.get(tracker.slots["pricerange"][0])
        cur_dif = 1
        suggested_price = ""

        while not valid_alternative and cur_dif > 2:
            next_priceranges = {
                key: value
                for key, value in priceranges.items()
                if value != current_pricerange
                and abs(value - current_pricerange) == cur_dif
            }
            current_slots = tracker.slots.copy()

            for price in next_priceranges:
                current_slots["pricerange"][0] = price
                available_restaurants = rest_database.findRestaurants(current_slots)
                if len(available_restaurants) > 0:
                    valid_alternative = True
                    suggested_price = price
                    break

            if not valid_alternative:
                cur_dif += 1

        if not valid_alternative:
            if tracker.slots["food"] is not None and len(tracker.slots["food"]) > 0:
                dispatcher.utter_message(
                    "Sorry i didn't find a good alternative, now looking for an alternative food type."
                )
                return [
                    {"event": "followup", "name": "action_suggest_alternative_food"}
                ]
            else:
                dispatcher.utter_message("Sorry i didn't find a good alternative.")
        elif priceranges.get(suggested_price) > current_pricerange:
            dispatcher.utter_message(
                "I found a more expensive restaurant. Is "
                + suggested_price
                + " also ok for you?"
            )
        elif priceranges.get(suggested_price) < current_pricerange:
            dispatcher.utter_message(
                "I found a cheaper restaurant. Is "
                + suggested_price
                + " also ok for you?"
            )
            # TODO: Only question here, need to look into how to handle response
        return []


class ActionSuggestAlternativeLocation(Action):
    def name(self) -> Text:
        return "action_suggest_alternative_location"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        return []


class ActionSuggestFilterBasedOnSlots(Action):
    def name(self) -> Text:
        return "action_suggest_filter_based_on_slots"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        if tracker.slots["pricerange"] is not None:
            alternative_price = find_price_alternative(tracker.slots)
            if alternative_price[0]:
                print(
                    {
                        "slot": "pricerange",
                        "suggestion": alternative_price[1],
                        "change": alternative_price[2],
                    }
                )
                # Alternative found
                return [
                    SlotSet(
                        "filter_suggestion",
                        {
                            "slot": "pricerange",
                            "suggestion": alternative_price[1],
                            "change": alternative_price[2],
                        },
                    )
                ]

        if tracker.slots["rest_location"] is not None:
            alternative_location = find_location_alternative(tracker.slots)
            if alternative_location[0]:
                print({"slot": "rest_location", "suggestion": alternative_location[1]})
                # Alternative found
                return [
                    SlotSet(
                        "filter_suggestion",
                        {
                            "slot": "rest_location",
                            "suggestion": alternative_location[1],
                        },
                    )
                ]

        if tracker.slots["food"] is not None:
            alternative_food = find_food_alternative(tracker.slots)
            if alternative_food[0]:
                print({"slot": "food", "suggestion": alternative_food[1]})
                # Alternative found
                return [
                    SlotSet(
                        "filter_suggestion",
                        {"slot": "food", "suggestion": alternative_food[1]},
                    )
                ]

            return [
                SlotSet(
                    "filter_suggestion",
                    {"slot": None, "suggestion": None},
                )
            ]


# pushes values from filter_suggestion to "real" slots
class ActionPushSuggestionsToQuerySlots(Action):
    def name(self):
        return "action_push_suggestions_to_query_slots"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ):
        slot_location = None
        slot_pricerange = None
        slot_food = None

        cur_suggestions = tracker.get_slot("filter_suggestion")
        print(cur_suggestions)
        if cur_suggestions:
            for slot in cur_suggestions:
                print(slot)
                if slot["slot"] == "food":
                    slot_food = slot["suggestion"]
                if slot["slot"] == "pricerange":
                    slot_pricerange = slot["suggestion"]
                if slot["slot"] == "rest_location":
                    slot_location = slot["suggestion"]

        print(f"Querying the DB with {slot_food}, {slot_pricerange}, {slot_pricerange}")

        return [
            SlotSet("food", slot_food),
            SlotSet("pricerange", slot_pricerange),
            SlotSet("rest_location", slot_location),
        ]


class ActionClearFoodFinderSlots(Action):
    def name(self) -> Text:
        return "action_clear_food_finder_slots"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        return [
            SlotSet("food", None),
            SlotSet("pricerange", None),
            SlotSet("area", None),
            SlotSet("postcode", None),
        ]


class ActionAppendHistory(Action):
    def name(self) -> Text:
        return "action_append_history"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:
        history = tracker.slots["search_history"]

        if history is None:
            history = []

        print("Appending slots to history:")
        print(tracker.slots)
        history.append(
            {
                "food": tracker.slots["food"],
                "pricerange": tracker.slots["pricerange"],
                "area": tracker.slots["area"],
                "postcode": tracker.slots["postcode"],
            }
        )

        return [SlotSet("search_history", history)]


def get_postcodes_in_radius(postcode, radius):
    dists = distances[["postcode", postcode]].sort_values(by=postcode)
    dists = dists[dists[postcode].between(0, radius)]
    return dists


def get_n_closest_postcodes(postcode, n):
    dists = distances[["postcode", postcode]].sort_values(by=postcode)
    dists = dists.head(n)
    return dists


def get_closest_food(source_name):
    source = food_graph.vs.select(name=source_name)

    if len(source) == 0:
        return []

    targets = food_graph.vs.select(db=True)

    source = list(map(lambda v: v.index, source))
    targets = list(map(lambda v: v.index, targets))

    dists = food_graph.distances(
        source=source, target=targets, weights="distance", mode="all"
    )

    df = pd.DataFrame(columns=["name", "distance"])

    for index, dist in enumerate(dists[0]):
        target = targets[index]
        name = food_graph.vs[target]["name"]

        df = pd.concat(
            [pd.DataFrame([[name, dist]], columns=df.columns), df], ignore_index=True
        )

    df = df.drop(df[df["name"] == source_name].index)

    df = df.sort_values(["distance"])

    return df


def find_food_alternative(current_slots) -> Tuple[bool, str]:
    valid_alternative = False
    alternative_foods = get_closest_food(current_slots["food"][0].lower())
    cur_idx = 0
    while not valid_alternative and cur_idx < len(alternative_foods):
        alternative_slots = current_slots.copy()
        alternative_slots["food"][0] = str(alternative_foods.iloc[cur_idx]["name"])
        available_restaurants = rest_database.findRestaurants(alternative_slots)
        if len(available_restaurants) == 0 and len(alternative_slots["food"]) > 0:
            cur_idx += 1
        else:
            valid_alternative = True

    if cur_idx >= len(alternative_foods):
        # No alternative found.
        return False, ""
    else:
        return True, str(alternative_foods.iloc[cur_idx]["name"])


def find_price_alternative(current_slots) -> Tuple[bool, str, str]:
    valid_alternative = False
    priceranges = {"cheap": 1, "moderate": 2, "expensive": 3}
    try:
        current_pricerange = priceranges.get(current_slots["pricerange"][0])
    except IndexError:
        current_pricerange = "cheap"
    cur_dif = 1
    suggested_price = ""

    while not valid_alternative and cur_dif > 2:
        next_priceranges = {
            key: value
            for key, value in priceranges.items()
            if value != current_pricerange
            and abs(value - current_pricerange) == cur_dif
        }
        alternative_slots = current_slots.copy()

        for price in next_priceranges:
            alternative_slots["pricerange"][0] = price
            available_restaurants = rest_database.findRestaurants(alternative_slots)
            if len(available_restaurants) > 0:
                valid_alternative = True
                suggested_price = price
                break

        if not valid_alternative:
            cur_dif += 1

    if not valid_alternative:
        return False, "", ""
    elif priceranges.get(suggested_price) > current_pricerange:
        return True, suggested_price, ">"
    elif priceranges.get(suggested_price) < current_pricerange:
        return True, suggested_price, "<"


def find_location_alternative(current_slots) -> Tuple[bool, str]:
    # TODO: needs to get implemented
    return (False, "")
