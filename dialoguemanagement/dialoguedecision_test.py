import unittest
from datastructures.dialogueact import DialogueAct
from datastructures.dialoguestate import DialogueState
from dialoguemanagement.dialoguedecision import DialogueDecider


class TestActOn(unittest.TestCase):
    def setUp(self):
        self.decider = DialogueDecider()

    def test_update(self):
        # DialogueState({"intent":"find.restaurant","area":"centre", "pricerange":"cheap"})
        state = DialogueState(
            {"intent": "find.restaurant", "area": "centre", "pricerange": "cheap"}
        )
        sysact = self.decider.actOn(state)

        self.assertIsInstance(sysact, DialogueAct)
        self.assertEqual("ask", sysact.intent)
        self.assertIn("foodtype", sysact.slots)
        self.assertIsNone(sysact.slots["foodtype"])
