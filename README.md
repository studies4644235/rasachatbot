# Running the bot

## Prerequesites

- Enter the `rasaModel` directory
- Setup a venv with the python version from `.python-version`
- Install the packages in `requirements.txt`
- Run `rasa train --domain domain`
- Enter `vue-rasa-chat` directory
- Get a Bing Spell Check API key at https://www.microsoft.com/en-us/bing/apis/bing-spell-check-api
- Rename `.env.example` to `.env` and insert your API key

## With Docker (recommended)

> **Note:**
> Make sure the model is trained (see prerequesites)

- Enter root directory
- Run `docker-compose build` to build the required images
- Run `docker-compose up` to start the stack
- `Ctrl + C` to stop, `docker-compose down` to remove

## Manually

### Rasa action server

- Enter `rasaModel` directory
- Run `rasa run actions`

### Rasa core

- Enter `rasaModel` directory
- Run `rasa run -m models --enable-api --cors "*" --debug`

### Rasa frontend

- Enter `vue-rasa-chat` directory
- Install the Node.js version specified in `.node-version`
- Install `yarn` with `npm install -g yarn`
- Build with `yarn build`
- Start with `yarn preview`
