import igraph as ig
import matplotlib.pyplot as plt
import pandas as pd

foodsAvailable = [
    "african",
    "asian oriental",
    "british",
    "chinese",
    "european",
    "french",
    "gastropub",
    "indian",
    "international",
    "italian",
    "japanese",
    "korean",
    "lebanese",
    "mediterranean",
    "mexican",
    "modern european",
    "north american",
    "portuguese",
    "seafood",
    "spanish",
    "thai",
    "turkish",
    "vietnamese",
]
foodsTotal = [
    "afghan",
    "australasian",
    "australian",
    "austrian",
    "basque",
    "belgian",
    "brazilian",
    "cantonese",
    "caribbean",
    "catalan",
    "cuban",
    "danish",
    "english",
    "eritrean",
    "german",
    "greek",
    "hungarian",
    "indonesian",
    "irish",
    "jamaican",
    "latin american",
    "malaysian",
    "middle eastern",
    "modern american",
    "modern global",
    "moroccan",
    "new zealand",
    "north african",
    "north indian",
    "persian",
    "polish",
    "polynesian",
    "romanian",
    "russian",
    "scandinavian",
    "scottish",
    "singaporean",
    "south african",
    "south indian",
    "sri lankan",
    "swedish",
    "swiss",
    "the americas",
    "tuscan",
    "venetian",
    "welsh",
    "world",
]
foodAttributes = ["vegetarian", "kosher", "halal", "creative", "unusual", "traditional"]

regionsAfrica = [
    "african",
    "north african",
    "central african",
    "south african",
    "west african",
    "east african",
]
regionsAsia = [
    "asian",
    "north asian",
    "central asian",
    "east asian",
    "south asian",
    "south east asian",
    "west asian",
]
regionsEurope = [
    "european",
    "east european",
    "north european",
    "south european",
    "west european",
    "central european",
]
regionsAmerica = ["the americas", "north american", "latin american", "caribbean"]
regionsOceania = ["oceanian", "australasian", "melanesian", "micronesian", "polynesian"]

g = ig.Graph()


def try_insert(name, db):
    try:
        g.vs.find(name=name)
    except:
        g.add_vertex(name, db=db)


for food in foodsAvailable:
    try_insert(food, True)
for food in foodsTotal:
    try_insert(food, False)
for region in regionsAfrica:
    try_insert(region, False)
for region in regionsAsia:
    try_insert(region, False)
for region in regionsEurope:
    try_insert(region, False)
for region in regionsAmerica:
    try_insert(region, False)
for region in regionsOceania:
    try_insert(region, False)

# World
g.add_edge("modern global", "international", distance=0)
g.add_edge("world", "international", distance=0)
g.add_edge("african", "international", distance=2)
g.add_edge("asian", "international", distance=2)
g.add_edge("european", "international", distance=2)
g.add_edge("oceanian", "international", distance=2)

# Africa
g.add_edge("north african", "african", distance=1)
g.add_edge("central african", "african", distance=1)
g.add_edge("south african", "african", distance=1)
g.add_edge("west african", "african", distance=1)
g.add_edge("east african", "african", distance=1)

g.add_edge("moroccan", "north african", distance=1)
g.add_edge("eritrean", "east african", distance=1)

# Asia
g.add_edge("central asian", "asian", distance=1)
g.add_edge("east asian", "asian", distance=1)
g.add_edge("south asian", "asian", distance=1)
g.add_edge("south east asian", "asian", distance=1)
g.add_edge("west asian", "asian", distance=1)
g.add_edge("north asian", "asian", distance=1)

g.add_edge("chinese", "central asian", distance=1)
g.add_edge("indian", "south asian", distance=1)
g.add_edge("sri lankan", "south asian", distance=1)
g.add_edge("thai", "south east asian", distance=1)
g.add_edge("vietnamese", "south east asian", distance=1)
g.add_edge("indonesian", "south east asian", distance=1)
g.add_edge("singaporean", "south east asian", distance=1)
g.add_edge("malaysian", "south east asian", distance=1)
g.add_edge("asian oriental", "south east asian", distance=1)
g.add_edge("korean", "east asian", distance=1)
g.add_edge("japanese", "east asian", distance=1)
g.add_edge("lebanese", "west asian", distance=1)
g.add_edge("persian", "west asian", distance=1)

g.add_edge("north indian", "indian", distance=0.5)
g.add_edge("south indian", "indian", distance=0.5)

g.add_edge("cantonese", "chinese", distance=0.5)
g.add_edge("afghan", "central asian", distance=1)
g.add_edge("afghan", "south asian", distance=1)

g.add_edge("russian", "north asian", distance=1.5)

# Europe
g.add_edge("scandinavian", "european", distance=1.5)
g.add_edge("north european", "european", distance=1)
g.add_edge("east european", "european", distance=1)
g.add_edge("south european", "european", distance=1)
g.add_edge("west european", "european", distance=1)
g.add_edge("central european", "european", distance=1)
g.add_edge("modern european", "european", distance=1)

g.add_edge("danish", "north european", distance=1)
g.add_edge("swedish", "north european", distance=1)

g.add_edge("danish", "scandinavian", distance=1)
g.add_edge("swedish", "scandinavian", distance=1)

g.add_edge("british", "north european", distance=1)
g.add_edge("irish", "british", distance=1)
g.add_edge("welsh", "british", distance=1)
g.add_edge("scottish", "british", distance=1)
g.add_edge("english", "british", distance=1)
g.add_edge("gastropub", "british", distance=1)

g.add_edge("french", "west european", distance=1)
g.add_edge("belgian", "west european", distance=1)

g.add_edge("german", "central european", distance=1)
g.add_edge("austrian", "central european", distance=1)
g.add_edge("swiss", "central european", distance=1)

g.add_edge("polish", "east european", distance=1)
g.add_edge("hungarian", "east european", distance=1)
g.add_edge("romanian", "east european", distance=1)
g.add_edge("russian", "east european", distance=1.5)

g.add_edge("portuguese", "south european", distance=1)
g.add_edge("spanish", "south european", distance=1)
g.add_edge("italian", "south european", distance=1)
g.add_edge("greek", "south european", distance=1)
g.add_edge("turkish", "south european", distance=1.5)

g.add_edge("turkish", "mediterranean", distance=1.5)
g.add_edge("greek", "mediterranean", distance=1)
g.add_edge("french", "mediterranean", distance=1)
g.add_edge("italian", "mediterranean", distance=1)
g.add_edge("spanish", "mediterranean", distance=1)

g.add_edge("catalan", "spanish", distance=0.5)
g.add_edge("basque", "spanish", distance=1.5)
g.add_edge("basque", "french", distance=1.5)

g.add_edge("tuscan", "italian", distance=0.5)
g.add_edge("venetian", "italian", distance=0.5)

# Americas
g.add_edge("the americas", "international", distance=1)
g.add_edge("north american", "the americas", distance=1)
g.add_edge("latin american", "the americas", distance=1)
g.add_edge("caribbean", "the americas", distance=1)

g.add_edge("modern american", "north american", distance=1)

g.add_edge("mexican", "latin american", distance=1)
g.add_edge("brazilian", "latin american", distance=1)

g.add_edge("cuban", "caribbean", distance=1)
g.add_edge("jamaican", "caribbean", distance=1)

# Oceania
g.add_edge("australasian", "oceanian", distance=1)
g.add_edge("micronesian", "oceanian", distance=1)
g.add_edge("melanesian", "oceanian", distance=1)
g.add_edge("polynesian", "oceanian", distance=1)

g.add_edge("australian", "australasian", distance=1)
g.add_edge("new zealand", "australasian", distance=1)

# Other
g = g.as_directed()

g.add_edge("seafood", "sri lankan", distance=1)
g.add_edge("seafood", "malaysian", distance=1)
g.add_edge("seafood", "japanese", distance=1)
g.add_edge("seafood", "portuguese", distance=1)
g.add_edge("seafood", "spanish", distance=1)
g.add_edge("seafood", "french", distance=1)
g.add_edge("seafood", "greek", distance=1)
g.add_edge("seafood", "thai", distance=1)
g.add_edge("seafood", "vietnamese", distance=1)
g.add_edge("seafood", "chinese", distance=1)
g.add_edge("seafood", "singaporean", distance=1)
g.add_edge("seafood", "korean", distance=1)
g.add_edge("seafood", "moroccan", distance=1)
g.add_edge("seafood", "south african", distance=1)
g.add_edge("seafood", "australian", distance=1)


def get_closest(source_name):
    source = g.vs.select(name=source_name)
    targets = g.vs.select(db=True)

    source = list(map(lambda v: v.index, source))
    targets = list(map(lambda v: v.index, targets))

    dists = g.distances(source=source, target=targets, weights="distance", mode="all")

    df = pd.DataFrame(columns=["name", "distance"])

    for index, dist in enumerate(dists[0]):
        target = targets[index]
        name = g.vs[target]["name"]

        df = pd.concat(
            [pd.DataFrame([[name, dist]], columns=df.columns), df], ignore_index=True
        )

    df = df.sort_values(["distance"])

    return df


print(get_closest("seafood"))

ig.Graph.write_graphmlz(g, "graph.graphmlz")

fig, ax = plt.subplots(figsize=(15, 15))
ig.plot(
    g,
    target=ax,
    layout="auto",  # print nodes in a circular layout
    vertex_size=0.4,
    vertex_color=["steelblue" if db else "salmon" for db in g.vs["db"]],
    vertex_frame_width=4.0,
    vertex_frame_color="white",
    vertex_label=g.vs["name"],
    vertex_label_size=7.0,
    edge_width=2,
    edge_label_size=7.0,
    edge_color="#7142cf",
    edge_label=g.es["distance"],
)

plt.show()
