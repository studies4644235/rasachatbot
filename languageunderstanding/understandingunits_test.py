import unittest
from datastructures.dialogueact import DialogueAct

from languageunderstanding.understandingunits import SimpleNLU


class TestNLU(unittest.TestCase):
    def setUp(self):
        self.nlu = SimpleNLU()

    def test_parse(self):
        diaAct = self.nlu.parse(
            "I am looking for a cheap restaurant in the city centre"
        )

        self.assertIsInstance(diaAct, DialogueAct)
        self.assertEqual("find.restaurant", diaAct.intent)
        self.assertIn("area", diaAct.slots)
        self.assertIn("pricerange", diaAct.slots)
        self.assertEqual("centre", diaAct.slots["area"])
        self.assertEqual("cheap", diaAct.slots["pricerange"])
