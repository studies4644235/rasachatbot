from datastructures.dialogueact import DialogueAct


class SimpleNLU(object):
    def __init__(self):
        pass

    def parse(self, input: str) -> DialogueAct:
        # "I am looking for a cheap restaurant in the city centre"
        # -> "find.restaurant(area=center, pricerange=cheap)"
        return DialogueAct("find.restaurant", {"area": "centre", "pricerange": "cheap"})
