import axios from 'axios'
import { jsonrepair } from 'jsonrepair'

export type RasaMessage = {
  recipient_id: string
  custom: RasaMessageCustomObject
}

export type RasaMessageCustomObject = {
  message: string
  payload?:
    | RasaQueryResult
    | RasaSearchHistory
    | RasaSlots
    | RasaFilterSuggestion
    | UnknownRasaPayload
}

export type UnknownRasaPayload = {
  type: RasaPayloadType
  data: Object
}

export type RasaQueryResult = {
  type: RasaPayloadType.QueryResult
  data: Array<RasaQueryResultEntry> | string
}

export type RasaQueryResultEntry = {
  addr: string
  area: string
  description: string
  food: string
  id: number
  name: string
  phone: string
  postcode: string
  pricerange: string
  signature: string
}

export type RasaSearchHistory = {
  type: RasaPayloadType.SearchHistory
  data: undefined
}

export type RasaSlots = {
  type: RasaPayloadType.Slots
  data: RasaSlotsEntry
}

export type RasaSlotsEntry = {
  food: Array<string> | string
  pricerange: Array<RasaSlotPricerange> | string
  area: Array<string> | string
  rest_location: Array<string> | string
}

export enum RasaSlotPricerange {
  Cheap = 'cheap',
  Moderate = 'moderate',
  Expensive = 'expensive'
}

export type RasaFilterSuggestion = {
  type: RasaPayloadType.FilterSuggestion
  data: Array<RasaFilterSuggestionEntry>
}

export type RasaFilterSuggestionEntry = {
  slot: string
  suggestion: string
}

export enum RasaPayloadType {
  QueryResult = 'query_result',
  SearchHistory = 'search_history',
  Slots = 'slots',
  FilterSuggestion = 'filter_suggestion'
}

function parseDirtyJson(json: string) {
  if (json !== 'None') {
    return JSON.parse(jsonrepair(json))
  }
  return json
}

// 💩🤮😭
export async function askRasa(message: string, user: string): Promise<RasaMessage[] | undefined> {
  try {
    const response = await axios({
      method: 'post',
      url: 'http://localhost:5005/webhooks/rest/webhook',
      data: {
        message: message,
        sender: user
      }
    })

    const messages: RasaMessage[] = response.data

    messages.map((message) => {
      if (message.custom.payload !== undefined) {
        switch (message.custom.payload.type) {
          case RasaPayloadType.QueryResult: {
            message.custom.payload.data = parseDirtyJson(
              message.custom.payload.data as string
            ) as RasaQueryResultEntry[]

            break
          }
          case RasaPayloadType.SearchHistory:
            throw new Error('not implemented')
          case RasaPayloadType.Slots: {
            const dataOld = message.custom.payload.data as RasaSlotsEntry

            const dataNew = dataOld
            dataNew.food = parseDirtyJson(dataOld.food as string) as Array<string>
            dataNew.area = parseDirtyJson(dataOld.area as string) as Array<string>
            dataNew.pricerange = parseDirtyJson(
              dataOld.pricerange as string
            ) as Array<RasaSlotPricerange>
            dataNew.rest_location = parseDirtyJson(dataOld.rest_location as string) as Array<string>

            message.custom.payload.data = dataNew as RasaSlotsEntry
            break
          }
          case RasaPayloadType.FilterSuggestion: {
            message.custom.payload.data = parseDirtyJson(
              message.custom.payload.data as string
            ) as Array<RasaFilterSuggestionEntry>
            break
          }
          default:
            throw new Error('unknown payload type')
        }
      }

      return message
    })

    return messages
  } catch (err) {
    return undefined
  }
}
