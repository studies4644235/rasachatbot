#!/bin/bash

echo "Script will randomly match lines from two different files."

echo "Enter filepath 1 and filepath 2 in the order you want the matching to occur."
read file1
read file2

outfile="matched.txt"
file_array=("$file1" "$file2")

max_lines_1=$(wc -l < "$file1")
max_lines_2=$(wc -l < "$file2")

# remove leading "- " from files
for i in "$file1" "$file2"
do
    sed 's/^[[:space:]]*-\([[:space:]]*\)\([^[:space:]]*\)/\2/' "$i" > tmpfile && mv tmpfile "$i"
    echo "$i has been processed"
done

# generate counter
if [ "$max_lines_1" -gt "$max_lines_2" ]; then
  c="$max_lines_1"
  echo "$c" "File 1 is bigger."
else
  c="$max_lines_2"
  echo "$c" "File 2 is bigger"
fi

# match file content line by line

# generate random line
generate_rnd_line() {
  file="$1"
  line_count=$(($(wc -l < "$file")))
  line=$((RANDOM % line_count + 1))
  awk -v line="$line" 'NR == line' "$file"
}
seed=$$
while [ "$c" -gt 0 ]
do
  ((seed++))
  RANDOM=$seed
  result1=("$(generate_rnd_line "$file1")")
  result2=("$(generate_rnd_line "$file2")")
  echo "$result1. $result2">>"$outfile"
  ((c--))

done

# remove lines that are double
uniq -i "$outfile" > tmpfile && mv tmpfile "$outfile"

# add '- ' to the beginning of each line in the output
sed 's/^$/d/g; s/^/- /g ; s/\.\s+\.//g; s/^\.//' "$outfile" > tmpfile && mv tmpfile "$outfile"
echo "Matching lines completed. Results are stored in $outfile."
