FROM python:3.10.9

WORKDIR /app

COPY . /app

RUN pip install -r requirements-docker.txt

EXPOSE 5055

ENTRYPOINT rasa run actions