import { TextBlock } from './message'
import type { FlaggedToken } from './spellchecker'

export function tokenize(text: string, flaggedTokens: FlaggedToken[]): TextBlock[] {
  const textBlocks: TextBlock[] = []

  if (flaggedTokens.length > 0) {
    let curFlaggedTokenIdx = 0
    let exitFlaggedAt = -1

    let curTokenChars: string[] = []

    for (let i = 0; i < text.length; i++) {
      const curFlaggedToken = flaggedTokens[curFlaggedTokenIdx]
      const char = text.charAt(i)

      if (i == curFlaggedToken.offset) {
        exitFlaggedAt = curFlaggedToken.offset + curFlaggedToken.token.length

        const token = curTokenChars.join('')

        textBlocks.push(new TextBlock(token, null))
        curTokenChars = []
      }
      if (i == exitFlaggedAt) {
        const token = curTokenChars.join('')

        textBlocks.push(new TextBlock(token, curFlaggedToken.suggestions[0].suggestion))
        curTokenChars = []

        if (flaggedTokens[curFlaggedTokenIdx + 1] !== undefined) curFlaggedTokenIdx += 1
      }

      curTokenChars.push(char)
    }
    const token = curTokenChars.join('')

    textBlocks.push(new TextBlock(token, null))
  } else {
    textBlocks.push(new TextBlock(text, null))
  }

  return textBlocks
}
